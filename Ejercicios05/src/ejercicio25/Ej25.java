package ejercicio25;

import java.util.Scanner;

public class Ej25 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		int gradosCentigrados;
		int gradosKelvin;
		char repetirProceso;
		boolean repetir = true;
		
		while (repetir) {
			System.out.println("Introduce los grados cent�grados.");
			gradosCentigrados = Integer.parseInt(input.nextLine());
			
			gradosKelvin = gradosCentigrados + 273;
			
			System.out.println(gradosCentigrados + " grados cent�grados son " + gradosKelvin + " grados kelvin.");
			
			System.out.println("�Desea repetir el proceso? S/N");
			repetirProceso = input.nextLine().charAt(0);
			
			if (repetirProceso != 'S' && repetirProceso != 's') {
				repetir = false;
			}
		}
		
		input.close();

	}

}
