package ejercicio11;

import java.util.Scanner;

public class Ej11 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce una contrase�a");
		String password = input.nextLine();
		
		for (int i = 0; i < password.length(); i++) {
			System.out.print(password.charAt(i) + " ");
		}
		
		System.out.println("\nIntroduce otra contrase�a");
		String password2 = input.nextLine();
		char caracter;
		int cantidadDeNumeros = 0;
		
		for (int i = 0; i < password2.length(); i++) {
			caracter = password2.charAt(i);
			if (caracter >= '0' && caracter <= '9') {
				cantidadDeNumeros++;	
			}
		}
		
		System.out.println("En la contrase�a hay " + cantidadDeNumeros + " n�meros.");
		
		System.out.println("Introduce otra contrase�a");
		String password3 = input.nextLine();
		char caracter2;
		boolean mayusculas = false;
		
		for (int i = 0; i < password3.length(); i++) {
			caracter2 = password3.charAt(i);
			if ((caracter2 >= 'A' && caracter2 <= 'Z') || caracter2 == '�') {
				mayusculas = true;
			}
		}	
		
		System.out.println("Hay mayusculas " + mayusculas);
		
		input.close();

	}

}
