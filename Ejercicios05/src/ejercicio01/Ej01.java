package ejercicio01;

import java.util.Scanner;

public class Ej01 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce un n�mero.");
		int numero = input.nextInt();
		
		for (int i = 1; i <= numero; i++) {
			System.out.println(i);
		}
		
		input.close();

	}

}
