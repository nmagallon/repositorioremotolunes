package ejercicio19a;

import java.util.Scanner;

public class Ej19 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		int numero = 0;
		int calculo = numero;
		boolean hayNegativo = false;
		boolean hayPar = false;
		
		for (int i = 0; i < 5; i++) {
			System.out.println("Escribe un n�mero");
			numero = Integer.parseInt(input.nextLine());
			
			calculo = calculo + numero;
			
			if (numero < 0) {
				hayNegativo = true;
			}
			
			if ((numero % 2) == 0) {
				hayPar = true;
			}
		}
		
		if (hayNegativo) {
			System.out.println("Se han introducido numeros negativos.");
		} else {
			System.out.println("No se han introducido numeros negativos.");
		}
		
		if (hayPar) {
			System.out.println("Se han introducido numeros pares.");
		} else {
			System.out.println("No se han introducido numeros pares.");
		}
		
		System.out.println();
		
		input.close();

	}

}
