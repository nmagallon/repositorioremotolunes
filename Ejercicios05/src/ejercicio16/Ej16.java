package ejercicio16;

import java.util.Scanner;

public class Ej16 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce la cantidad de sueldos que va a haber.");
		int cantidadSueldos = Integer.parseInt(input.nextLine());
		
		int sueldo;
		int sueldoMayor = 0;
		
		for (int i = 0; i < cantidadSueldos; i++) {
			System.out.println("Introduce un sueldo.");
			sueldo = Integer.parseInt(input.nextLine());
			sueldoMayor = Math.max(sueldo, sueldoMayor);
		}
		
		System.out.println("El sueldo mayor es " + sueldoMayor);
		
		input.close();
		
	}

}
