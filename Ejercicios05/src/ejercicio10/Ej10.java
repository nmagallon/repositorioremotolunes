package ejercicio10;

import java.util.Scanner;

public class Ej10 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int numero;
		int numeroCalculo = 0;
		
		for (int i = 1; i <= 10; i++) {
			System.out.println("Introduce un n�mero");
			numero = Integer.parseInt(input.nextLine());
			numeroCalculo = numeroCalculo + numero;
		}
		
		System.out.println("La suma total es " + numeroCalculo);
		
		input.close();

	}

}
