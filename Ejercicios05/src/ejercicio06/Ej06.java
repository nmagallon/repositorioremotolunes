package ejercicio06;

import java.util.Scanner;

public class Ej06 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Escribe un numero.");
		int numero = input.nextInt();
		int contador = 0;
		
		while (numero != 0) {
			numero /= 10;
			contador += 1;
		}
		
		System.out.println(contador + " cifras.");
		
		input.close();
		

	}

}
