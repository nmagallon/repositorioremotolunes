package ejercicio20a;

import java.util.Scanner;

public class Ej20 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int numero;
		boolean hayNegativo = false;
		
		do {
			System.out.println("Introduce un n�mero.");
			numero = Integer.parseInt(input.nextLine());
			
			if (numero < 0) {
				hayNegativo = true;
			}
		} while (numero != 0);
		
		if (hayNegativo) {
			System.out.println("Hay n�meros negativos.");
		} else {
			System.out.println("No hay n�meros negativos.");
		}
		
		input.close();

	}

}
