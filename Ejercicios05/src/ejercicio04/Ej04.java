package ejercicio04;

import java.util.Scanner;

public class Ej04 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce un n�mero.");
		int num1 = input.nextInt();
		input.nextLine();
		
		System.out.println("Introduce un n�mero menor");
		int num2 = input.nextInt();
		
		for (int i = num1; i >= num2; i--) {
			System.out.println(i);
		}
		
		input.close();

	}

}
