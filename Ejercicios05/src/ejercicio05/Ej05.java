package ejercicio05;

import java.util.Scanner;

public class Ej05 {

	public static void main(String[] args) {
		
		int numeroInicial, numeroFinal;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce un n�mero.");
		numeroInicial = input.nextInt();
		input.nextLine();
		
		System.out.println("Introduce otro n�mero.");
		numeroFinal = input.nextInt();
		
		if (numeroInicial > numeroFinal) {
			for (int i = numeroInicial; i >= numeroFinal; i--) {
				System.out.println(i);
			}
		} else {
			for (int i = numeroFinal; i >= numeroInicial; i--) {
				System.out.println(i);
			}
		}
		
		input.close();

	}

}
