package ejercicio18;

import java.util.Scanner;

public class Ej18 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce el lado del cuadrado.");
		int lado = Integer.parseInt(input.nextLine());
		
		for (int i = 1; i <= lado; i++) {
			for (int j = 1; j <= lado; j++) {
				System.out.print("X ");
			}
			System.out.print("\n");
		}
		
		input.close();

	}

}
