package ejercicio03;

import java.util.Scanner;

public class Ej03 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Escribe un n�mero mayor a 1.");
		int numeroLeido = input.nextInt();
		int numeroBucle = numeroLeido;
		
		do {
			System.out.println(numeroBucle);
			numeroBucle--;
		} while (numeroBucle >= 1);
		
		numeroBucle = numeroLeido;
		
		do {
			System.out.print(numeroBucle + " ");
			numeroBucle--;
		} while (numeroBucle >= 1);
		
		input.close();

	}

}
