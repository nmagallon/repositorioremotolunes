package ejercicio27;

import java.util.Scanner;

public class Ej27 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner (System.in);
		
		System.out.println("Introduce una cadena de texto.");
		String cadenaLeida = input.nextLine().toLowerCase();
		
		int contadorVocales = 0;
		int porcentajeVocales;
		
		for (int i = 0; i < cadenaLeida.length(); i++) {
			System.out.print(cadenaLeida.charAt(i) + " ");
			if (cadenaLeida.charAt(i) == 'a' 
					|| cadenaLeida.charAt(i) == 'e'
					|| cadenaLeida.charAt(i) == 'i'
					|| cadenaLeida.charAt(i) == 'o'
					|| cadenaLeida.charAt(i) == 'u') {
				contadorVocales++;
			}
		}
		
		porcentajeVocales = (contadorVocales * 100) / cadenaLeida.length();
		
		System.out.println("\nHay un " + porcentajeVocales + "% de vocales en la cadena.");
		
		input.close();

	}

}
