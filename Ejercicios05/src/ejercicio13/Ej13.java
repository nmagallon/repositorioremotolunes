package ejercicio13;

import java.util.Scanner;

public class Ej13 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Escribe un n�mero.");
		int base = Integer.parseInt(input.nextLine());
		
		System.out.println("Escribe el exponente.");
		int exponente = Integer.parseInt(input.nextLine());
		
		int calculo = 1;
		
		for (int i = 1; i <= exponente; i++ ) {
			calculo = calculo * base;
		}
		
		System.out.println(calculo);
		
		input.close();

	}

}
