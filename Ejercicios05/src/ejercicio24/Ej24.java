package ejercicio24;

import java.util.Scanner;

public class Ej24 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce el alto y el ancho del rectángulo.");
		int alto = input.nextInt();
		int ancho = input.nextInt();
		
		for (int i = 0; i < alto; i++) {
			for (int j = 0; j < ancho; j++) {
				System.out.print("x ");
			}
			System.out.println();
		}
		
		input.close();

	}

}
