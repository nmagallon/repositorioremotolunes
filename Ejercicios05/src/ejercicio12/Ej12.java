package ejercicio12;

import java.util.Scanner;

public class Ej12 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		int opcion = 0;
		
		while (opcion != 3) {
			System.out.println("1. Opcion 1"
					+ "\n" + "2. Opcion 2"
					+ "\n" + "3. Salir"
					+ "\n" + "Introduce una opcion.");
			opcion = Integer.parseInt(input.nextLine());
			
			switch (opcion) {
				case 1:
					System.out.println("Has escogido la opcion 1.");
				break;
				case 2:
					System.out.println("Has escogido la opcion 2.");
				break;
			}
		}
		
		System.out.println("Hasta luego");
		
		input.close();

	}

}
