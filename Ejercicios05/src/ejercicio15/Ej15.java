package ejercicio15;

import java.util.Scanner;

public class Ej15 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int numeroLeido;
		int calculo = 0;
		int contador = 0;
		
		do {
			System.out.println("Introduce un n�mero entero positivo o negativo.");
			numeroLeido = Integer.parseInt(input.nextLine());
			calculo = calculo + numeroLeido;
			contador++;
		} while (numeroLeido != 0);
		
		System.out.println("El resultado de la suma de todos los n�meros introducidos es " + calculo);
		System.out.println("Se han introducido " + contador + " n�meros.");
		
		input.close();
	}

}
