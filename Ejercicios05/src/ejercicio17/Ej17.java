package ejercicio17;

import java.util.Scanner;

public class Ej17 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce 10 n�meros.");
		int numero;
		boolean numeroNegativo = false;
				
		for (int i = 0; i < 10; i++) {
			numero = Integer.parseInt(input.nextLine());
			if (numero < 0) {
				numeroNegativo = true;
			}	
		}
		
		if (numeroNegativo == true) {
			System.out.println("Hay n�meros negativos");
		} else {
			System.out.println("No hay n�meros negativos");
		}
		
		input.close();
		
	}

}
