package ejercicio23;

import java.util.Scanner;

public class Ej23 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		double notaLeida;
		double notaTotal = 0;
		int contadorNotas = 0;
		double notaMedia;
		
		do {
			System.out.println("Escribe la nota de un alumno.");
			notaLeida = Double.parseDouble(input.nextLine());
			
			if (notaLeida >= 0) {
				notaTotal = notaTotal + notaLeida;
				contadorNotas++;
			}
			
		} while (notaLeida > 0);
		
		notaMedia = notaTotal / contadorNotas;
		
		System.out.println(notaMedia);
		
		input.close();

	}

}
