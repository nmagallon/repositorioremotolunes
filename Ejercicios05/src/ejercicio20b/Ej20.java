package ejercicio20b;

import java.util.Scanner;

public class Ej20 {
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Escribe el caracter que quieres buscar.");
		char caracterBuscado = input.nextLine().charAt(0);
		
		System.out.println("Escribe la cadena en la que quieres buscar el caracter.");
		String cadenaLeida = input.nextLine();
		
		int contador = 0;
		
		for (int i = 0; i < cadenaLeida.length(); i++) {
			if (cadenaLeida.charAt(i) == caracterBuscado) {
				contador++;
			}
		}
		
		System.out.println("Hay " + contador + " repeticiones del caracter.");
		
		input.close();
	
	}
}
