package ejercicio21;

import java.util.Scanner;

public class Ej21 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Escribe un n�mero en binario de 8 cifras.");
		int numeroBinario = Integer.parseInt(input.nextLine());
		
		double numeroDecimal = 0;
		
		
		for (int i = 0; i < 8; i++) {
			numeroDecimal = numeroDecimal + (numeroBinario % 10 * Math.pow(2, i));
			numeroBinario /= 10;
		}
		
		System.out.println(numeroDecimal);
		
		input.close();

	}

}
