package ejercicio22;

import java.util.Scanner;

public class Ej22 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce un n�mero binario.");
		long numeroBinario = Long.parseLong(input.nextLine());
		
		double numeroDecimal = 0;
		int exponente = 0;
		
		while (numeroBinario > 0) {
			numeroDecimal = numeroDecimal + numeroBinario % 10 * (int)Math.pow(2, exponente);
			exponente++;
			numeroBinario /= 10;
		}
		
		System.out.println(numeroDecimal);
		
		input.close();

	}

}
