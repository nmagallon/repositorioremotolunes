package ejercicio14;

import java.util.Scanner;

public class Ej14 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce la capacidad total del dep�sito");
		int capacidad = Integer.parseInt(input.nextLine());
		
		System.out.println("Introduce la cantidad de litros de agua que hay en el dep�sito");
		int cantidad = Integer.parseInt(input.nextLine());
		
		int cantidadRestante = capacidad - cantidad;
		int cantidadIntroducida;
		
		while (cantidad <= capacidad) {
			System.out.println("Faltan " + cantidadRestante + " litros para llenar el dep�sito.");
			System.out.println("Introduce la cantidad de litros que quieres sumar o restar.");
			cantidadIntroducida = Integer.parseInt(input.nextLine());
			cantidad = cantidad + cantidadIntroducida;
			cantidadRestante = capacidad - cantidad;
		}
		
		System.out.println("El dep�sito est� lleno.");
		
		input.close();

	}

}
