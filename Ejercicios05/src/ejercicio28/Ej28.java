package ejercicio28;

import java.util.Scanner;

public class Ej28 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		int numeroLeido;
		int numeroMayor = 0;
		int numeroMenor = 0;
		
		do {
			System.out.println("Introduce un n�mero.");
			numeroLeido = Integer.parseInt(input.nextLine());
			
			if (numeroLeido == 0) {
				break;
			}
			
			numeroMayor = Math.max(numeroMayor, numeroLeido);
			numeroMenor = Math.min(numeroMayor, numeroLeido);
		} while (numeroLeido != 0);
		
		System.out.println("El n�mero mayor es " + numeroMayor);
		System.out.println("El n�mero menor es " + numeroMenor);
		
		input.close();

	}

}
