package ejercicio19b;

import java.util.Scanner;

public class Ej19 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		int numeroLeido;
		boolean hayMultiploTres = false;
		
		for (int i = 0; i < 5; i++) {
			System.out.println("Introduce un n�mero.");
			numeroLeido = Integer.parseInt(input.nextLine());
			
			if (numeroLeido % 3 == 0) {
				hayMultiploTres = true;
			}
		}
		
		if (hayMultiploTres) {
			System.out.println("Hay m�ltiplos de tres.");
		} else {
			System.out.println("No hay m�ltiplos de tres.");
		}
		
		input.close();
		
	}

}
