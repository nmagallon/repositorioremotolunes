package ejercicio26;

import java.util.Scanner;

public class Ej26 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce un n�mero de 0 a 255.");
		
		int numeroDecimal = Integer.parseInt(input.nextLine());
		int numeroBinario = 0;
		
		while (numeroDecimal != 0) {
			numeroBinario = numeroBinario * 10 + numeroDecimal % 2;
			numeroDecimal /= 2;
		}
		
		System.out.println(numeroBinario);
		
		input.close();		

	}

}
